"""
UniversVideo 

"""

__version__ = '0.2'

from re import search, sub
from os.path import split as splitpath
from os.path import splitext, abspath, dirname, exists, getmtime
from subprocess import Popen, PIPE
from math import floor
from os import makedirs, mkdir
from httplib import HTTPConnection
from urlparse import urlsplit, urlparse
from copy import copy

def check_for_ffmpeg():
    deps = {'ffmpeg':['/usr/local/bin/ffmpeg', '/usr/bin/ffmpeg'],
            'ffmpeg2theora': ['/usr/local/bin/ffmpeg2theora', '/usr/bin/ffmpeg2theora']}
    for application, paths in deps.items():
        installed = False
        for path in paths:
            if exists(path):
                installed = True
        if installed == False:
            raise UniversException('%s is not installed' % application)


class UniversException(Exception):
    pass


class UniversUtils:

    def html_escape(self, text):
        """ Returns a string that has html special characters escaped """

        chars = {"&": "&amp",'"': "&quot","'": "&apos",">": "&gt","<": "&lt"}
        return ''.join([chars.get(c,c) for c in text])

    def path_info(self, path):
        """ Returns a dictionary of path information """

        filepath, extension = splitext(path)
        directory, filename = splitpath(filepath)
        return {'dirname': directory, 'basename': '.'.join([filename, extension]),
                'extension': extension, 'filename': filename}

    def check_string(self, s):
        """ Checks to see if it is a string and escapse html
        special characters. 
        """

        if not type(s) == str:
            raise UniversException('Please use a string here')
        return self.html_escape(s)

    def check_width(self, width):
        """ Checks to see if the width is an Integer, is divisible by 2,
        and isn't larger than 1440 
        """

        if not type(width) == int:
            raise UniversException('Please use an Integer for the width.')
        if width %2 > 0:
            raise UniversException('Please use a number divisible by 2')
        if width > 1440:
            raise UniversException('Please use a width below 1440.')
        return width

    def check_url(self, url, head=True):
        """ Checks to make sure that the URL has a scheme, and optionally
        will HEAD the url to see if there is a sucessful status code. 
        """

        if not urlparse(url)[0]:
            raise UniversException('Please use http:// (or other scheme) infront of the url: %s' % url)

        if head:
            urlinfo = urlsplit(url)
            con = HTTPConnection(urlinfo[1])
            path_and_query = urlinfo[2]
            if len(urlinfo[3]) > 0:
                path_and_query += '?' + urlinfo[3]
            con.request('HEAD', path_and_query)
            res = con.getresponse()
            if res.status > 399:
                raise UniversException('The url "%s" got status code: %s.' % (url, res.status))

        if url[-1] == '/':
            raise UniversException('Please do not use a trailing slash on the end of the URL')

        return url


class UniversVideoSettings:
    """ Object to store and validate settings to be passed to UniversVideo 

    width                The width of the full size video, ie: 640
    embed_width          The width of the 'embed' version of the video. ex: 480
    site_url             The url of your site. ex: "http://example.com"
    site_title           The title of the site. ex: "Your Site"
    video_base_path      The absolute path to the video files. ex: "/var/www/example.com/video"
    video_base_url       The full url to the video files. ex: "http://example.com/video/"
    font_family          The typeface to use. ex: "Arial, sans-serif"
    font_size            An integer in pixels. ex: 13
    bgcolor              A Hexidecimal value for the color. ex: "ffffff"
    static_base_url      The full url to the univers shared static files. ex: "http://example.com/shared/univers"

    """

    def __init__(self, width, embed_width, site_url,
                 site_title, video_base_path, video_base_url,
                 font_family, font_size, bgcolor, static_base_url):

        utils = UniversUtils()

        #check the width settings
        self.width = utils.check_width(width)
        if embed_width:
            self.embed_width = utils.check_width(embed_width)

        self.widths = [self.width]
        if not self.width == self.embed_width:
            self.widths.append(self.embed_width)

        #check site title and font family for quotes, and html special chars.
        self.site_title = utils.check_string(site_title)
        self.font_family = utils.check_string(font_family)

        #check the font_size setting
        if not type(font_size) == int:
            raise UniversException('Please use an Integer for the size of the font in pixels.') 
        self.font_size = font_size

        #check the bgcolor setting
        if not type(bgcolor) == str:
            raise UniversException('Please use a string for the color')
        if not len(str(bgcolor)) == 6:
            raise UniversException('Please choose the color using six characters. Example: ffffff')
        hex_values = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
        for char in bgcolor.lower():
            if not char in hex_values:
                raise UniversException('Please use a only the characters A-Z, and 1-9 to spefity the hexidecimal color value.')
        self.bgcolor = bgcolor

        #no trailing slash?
        if video_base_path[-1] == '/':
            raise UniversException('Please do not use a trailing slash')

        #video_base_path exists? if not try to create it.
        if not exists(video_base_path):
            makedirs(video_base_path)

        self.video_base_path = video_base_path
        
        queue_file = video_base_path + '/queue'
        if not exists(queue_file):
            f = open(queue_file, 'w')
            f.close()

        running_file = video_base_path + '/running'
        if not exists(running_file):
            f = open(running_file, 'w')
            f.close()

        for w in self.widths:
            subdir = '/'.join([video_base_path, str(w)])
            if not exists(subdir):
                mkdir(subdir)

        #check the urls
        self.site_url = utils.check_url(site_url, head=False)
        self.video_base_url  = utils.check_url(video_base_url)
        self.static_base_url = utils.check_url(static_base_url)

        #check to see if the flash player exists and is up-to-date
        swf_file = '/usr/share/univers/univers-%s.swf' % __version__
        if not exists(swf_file):
            #we would build it using the ming python extension, 
            #but it is a pain in the ass, so we will use php
            #try:
            #    import ming
            #except ImportError:
            #    UniversException('You do not have "ming", and "python-ming" installed. It is needed to build a new flash player.')
            #from scripts import univers_build_swf
            #univers_build_swf.make()
            p = Popen('univers_build_swf.php %s' % __version__ , 
                      shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)

        if not exists(swf_file):
            raise UniversException('There was a problem creating the univers swf file at %s Try manually running "univers_build_swf.php %s"' % (swf_file, __version__))

        if getmtime('/usr/share/univers/univers.as') > getmtime(swf_file):
            raise UniversException('The univers.as is newer than the last built version of the swf player file, increase your version number to build a new version.')


class UniversVideo:

    """ Instances of UniversVideo will automatically process video based on the 
    settings passed to it, and can generate the html nessasary to view the 
    video component in a web browser; includes, a JavaScript based HTML 5 video 
    playback, and an ActionScript Flash implementation with the same 
    video controls interface. 
    """

    def __init__(self, source, title, permalink, embed_url, fullscreen_url, settings):

        utils = UniversUtils()

        if not exists(source):
            raise UniversException('File %s does not exists' % source)
        self.source = source

        if not isinstance(settings, UniversVideoSettings):
            raise UniversException('The parameter "settings" is not an instance of UniversVideoSettings')
        self.settings = settings

        self.title = utils.html_escape(title)
        self.permalink = utils.check_url(permalink, head=False)
        self.embed_url = utils.check_url(embed_url, head=False)
        self.fullscreen_url = utils.check_url(fullscreen_url, head=False)

        self.source_info = utils.path_info(source)
        self.meta = self.get_video_meta()

        if self.completed(): 
            self.completed = True
        else:
            self.completed = False
            if not self.queued() or not self.processing():
                self.process_video()

    def html(self, standalone=True, embed=False):
        width = (self.settings.width, self.settings.embed_width)[embed]
        html = ''
        height = int(width / float(self.meta['width']) * float(self.meta['height']))

        if standalone and not embed:
            fullscreen = 'true'
            autoplay = 'true'

        if standalone and embed:
            fullscreen = 'false'
            autoplay = 'false'

        if not standalone and embed:
            fullscreen = 'false'
            autoplay = 'true'

        if not standalone and not embed:
            fullscreen = 'false'
            autoplay = 'true'

        searchlist = copy(self.settings.__dict__)
        searchlist.update({
            'version': __version__,
            'width': width,
            'height': height,
            'heightplus': height+15,
            'embed_width': self.settings.embed_width,
            'embed_height': int(self.settings.embed_width / float(self.meta['width']) * float(self.meta['height'])),
            'id': sub("[\W_]", "", self.source_info['filename']),
            'autoplay':autoplay,
            'fullscreen':fullscreen,
            'base_url': self.settings.static_base_url,
            'base_file': '/'.join([self.settings.video_base_url, str(width), self.source_info['filename']]),
            'title': self.title,
            'permalink': self.permalink,
            'object_url': self.embed_url,
            'fullscreen_url': self.fullscreen_url,
            'bgcolor': self.settings.bgcolor,
            })

        if standalone:
            html += """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                                          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd ">"""
            html += """<html><head>
                     <title>%(title)s</title>
                     <script type="text/javascript" src="%(static_base_url)s/univers.js"></script>
                     <style>img{border:0;}body{font-family:Courier, Courier New, monospace;color:#fff;margin:0;}body,html{overflow:hidden;border:0;background-color:#000;}.universvideo{margin:auto}a{color:#fff;}</style>
                     </head><body>""" % searchlist

        if self.completed:
            html += """
            <!-- "UniversVideo" by Braydon Fuller <video.braydon.com> GPL -->
            <div class="universvideo"
                 id="video_%(id)s"
                 style="width:%(width)spx;">

                <!-- "Video for Everybody" by Kroc Camen <camendesign.com> cc-by -->
                <video width="%(width)s" height="%(height)s" controls="controls" poster="%(base_file)s.jpg">
                    <source src="%(base_file)s.ogg" type="video/ogg" />
                    <source src="%(base_file)s.mp4" type="video/mp4" />
                    <object width="%(width)s" height="%(height)s" type="application/x-shockwave-flash"
                            data="%(base_url)s/univers-%(version)s.swf" bgcolor="%(bgcolor)s" flashvars="autoplay=%(autoplay)s&width=%(width)s&height=%(height)s&embed_width=%(embed_width)s&embed_height=%(embed_height)s&base_url=%(base_url)s&base_file=%(base_file)s&video_title=%(title)s&video_url=%(base_file)s.mp4&site_title=%(site_title)s&site_url=%(site_url)s&bgcolor=%(bgcolor)s&object_url=%(object_url)s&font_size=%(font_size)s&font_family=%(font_family)s&flash_url=%(base_url)s/univers-%(version)s.swf" wmode="transparent">
                    <param name="movie" value="%(base_url)s/univers-%(version)s.swf" />
                    <param name="wmode" value="transparent" /> 
                    <param name="bgcolor" value="%(bgcolor)s" />
                    <param name="flashvars" value="autoplay=%(autoplay)s&width=%(width)s&height=%(height)s&embed_width=%(embed_width)s&embed_height=%(embed_height)s&base_url=%(base_url)s&base_file=%(base_file)s&video_title=%(title)s&video_url=%(base_file)s.mp4&site_title=%(site_title)s&site_url=%(site_url)s&bgcolor=%(bgcolor)s&object_url=%(object_url)s&font_size=%(font_size)s&font_family=%(font_family)s&flash_url=%(base_url)s/univers-%(version)s.swf" />
                    <!--[if gt IE 6]>
                    <object width="%(width)s" height="%(heightplus)s" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B">
                    <param name="autoplay" value="%(autoplay)s" />
                    <param name="src" value="%(base_file)s.mp4" /><!
                    [endif]--><!--[if gt IE 6]><!-->
                    <object width="%(width)s" height="%(heightplus)s" type="video/quicktime" data="%(base_file)s.mp4">
                    <param name="autoplay" value="%(autoplay)s" />
                    <param name="src" value="%(base_file)s.mp4" />
                    <!--<![endif]-->
                    <img src="%(base_file)s.jpg" alt="%(title)s" />
                    <p>Download Video: <a href="%(base_file)s.ogg">Ogg Theora format</a> |
                    <a href="%(base_file)s.mp4">MPEG-4 format</a> 
                    </p><!--[if gt IE 6]><!--></object><!--<![endif]-->
                    <!--[if gt IE 6]></object><![endif]-->
                    </object>
                </video>
                <!-- End "Video for Everybody" -->
            </div>""" % searchlist


            if standalone and not embed:
                html += """
                <div class="universvideoinfo" style="width:%(width)spx;clear:both;margin:10px auto 0 auto;">
                    <div style="float:left;"><a href="%(permalink)s">Back</a> | %(title)s </div>  <div style="float:right;"><a href="%(site_url)s" title="%(site_title)s"><img src="%(base_url)s/logo_rev.png" alt="%(site_title)s"/></a></div>
                </div>
                """ % searchlist


            if embed:
                html += """
                <p class="universvideoinfo" style="width:%(width)spx;">
                    <a href="%(fullscreen_url)s">View Larger</a>
                </p>""" % searchlist

            html += """
            <script>
               <!--
               video_%(id)s = new Video({
               'id':'video_%(id)s',
               'size':[%(width)s,%(height)s],
               'embed_size':[%(embed_width)s,%(embed_height)s],
               'base_url':'%(base_url)s',
               'base_file':'%(base_file)s',
               'flash':'%(base_url)s/univers-%(version)s.swf',
               'autoplay':%(autoplay)s,
               'fullscreen':%(fullscreen)s,
               'bgcolor':'%(bgcolor)s',
               'display':{
                   'font_family':'%(font_family)s',
                   'font_size':'%(font_size)s',
                   'site_title':'%(site_title)s',
                   'site_url':'%(site_url)s',
                   'video_title':'%(title)s',
                   'video_url':'%(permalink)s',
                   'object_url':'%(object_url)s'
                }});
                //-->
            </script>
            <!-- End "UniversVideo" -->
            """ % searchlist

        else:
            html += "Video is Processing, Check back soon." % searchlist

        if standalone:
            html += "<body></html>"

        return html

    def process_video(self):
        check_for_ffmpeg()
        start_snapshot_at = self.meta['duration']/2

        for width in self.settings.widths:
            height = int(round(float(width) / float(self.meta['width']) * float(self.meta['height'])))
            if height % 2 > 0:
                height = 2 * floor(height / 2)
            dst_base = '/'.join([self.settings.video_base_path, str(width), self.source_info['filename']])

            def in_running(file):
                for line in open(file, 'r').readlines():
                    if line.find(self.source) > -1:
                        return True
                return False

            def in_queue(file):
                for line in open(file, 'r').readlines():
                    if line.find(self.source + ' ' + str(width)) > -1:
                        return True
                return False

            if not in_queue(self.settings.video_base_path + '/queue') and \
                    not in_running(self.settings.video_base_path + '/running'):
                queue = open(self.settings.video_base_path + '/queue', 'a')
                queue.write(' '.join([str(x) for x in [self.source, width, height,
                                                 start_snapshot_at, dst_base, 
                                                 self.settings.video_base_path,
                                                 self.meta['width'],
                                                 self.meta['height']]])+'\n')
                queue.close()

        self.queue_daemon()

    def get_video_meta(self):
        if not exists(self.source):
            raise UniversException('No such file: %s' % self.source)

        p = Popen("ffmpeg -i %s" % self.source, shell=True, stdin=PIPE, 
                  stdout=PIPE, stderr=PIPE, close_fds=True)

        message = p.stderr.read()
        if message.find('FFmpeg') == -1:
            check_for_ffmpeg()

        sizestr = search('\d+x\d+,', message).group(0)[:-1]
        dur = search('Duration:\s\d\d:\d\d:\d\d\.\d\d', message).group(0).replace('Duration: ','').split(':')
        duration = int(round(float(dur[0]) * 3600 + float(dur[1]) * 60 + float(dur[2])))
        width, height = sizestr.split('x')
        return {'width':int(width), 'height':int(height), 'duration':int(duration)}

    def completed(self):
        orgmtime = getmtime(self.source)

        for width in self.settings.widths:
            sb = '/'.join([self.settings.video_base_path, str(width), self.source_info['filename']])
            extensions = ['.ogg', '.jpg', '.mp4']
            for ext in extensions:
                if not exists(sb + ext) or getmtime(sb + ext) < orgmtime:
                    return False
        return True

    def processing(self):
        running_file = open(self.settings.video_base_path + '/running', 'r')
        if running_file.readline().find(self.source) > -1:
            return True
        return False

    def queued(self):
        queue_file = open(self.settings.video_base_path + '/queue', 'r')
        for line in queue_file.readlines():
            if line.find(self.source) > -1:
                return True
        return False

    def queue_daemon(self):
        cmd = "univers_start.sh %s" % self.settings.video_base_path
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, 
                  stderr=PIPE, close_fds=True)


